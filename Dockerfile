# Se declara imagen base de node a usar
FROM node:lts-alpine

# Se crea el directorio app y se asigna al usuario node
RUN mkdir /home/node/app && chown node:node -R /home/node/app

# Se ingresa al directorio
WORKDIR /home/node/app

# Se asigna el usuario node
USER node

# Se copian los archivos necesarios en el directorio app con permisos de usuario node
COPY --chown=node:node "package*.json" "index.js" /home/node/app/

# Se ejecuta npm install y se limpia la cache para reducir tamaño.
RUN npm install --production && npm cache clean --force

# Se expone el puerto 3000
EXPOSE 3000

CMD ["node", "index.js"]